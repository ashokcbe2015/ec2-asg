################## Below are improvements to be done in current setup and architecture  ######################

1. Need to add SSL termination at Application load balancer for adding in-transit encryption
2. Add sns notification whenever Instance refresh occurs
3. Monitor resources using cloudwatch
4. Enable VPC endpoint for s3 service to avoid exposing traffic to internet