terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "ap-southeast-1"
}


resource "aws_vpc" "test_vpc" {
  # Default VPC was importd into terraform 
}


resource "aws_subnet" "private_subnet" {
  vpc_id                  = var.vpc_id
  count                   = 1
  cidr_block              = var.cidr_block
  availability_zone       = "ap-southeast-1a"
  map_public_ip_on_launch = false

  tags = {
    Name        = "private-subnet-01"
  }
}

resource "aws_nat_gateway" "example" {
  allocation_id   =   var.allocation_id
  subnet_id     = var.subnet_id

  tags = {
    Name = "Nginx NAT"
  }
}

resource "aws_route_table" "private" {
  vpc_id = var.vpc_id
   route {
        cidr_block = "0.0.0.0/0"
        nat_gateway_id = var.nat_gateway_id
    }
  tags = {
    Name        = "private-route-table-01"
  }
}

resource "aws_route_table_association" "private" {
  count          = 1
  subnet_id      = var.subnet_id
  route_table_id = var.route_table_id
}