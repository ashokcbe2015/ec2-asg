variable "vpc_id" {
	default = "vpc-01d2ef58d0e16a146"
}

variable "cidr_block" {
	default = "172.31.48.0/20"
}


variable "allocation_id" {
	default = "eipalloc-0ee64af8bb1029bb6"
}


variable "nat_gateway_id" {
	default = "nat-064747025ad4ed44a"
}

variable "route_table_id" {
	default = "rtb-09765a858262c5852"
}

variable "subnet_id" {
	default = "subnet-0e050138f1f715c28"
}